---
layout: markdown_page
title: Sign-off
category: Project Execution
---

After the SOW has been completed, the customer will need to sign a [Project sign off document](https://docs.google.com/document/d/1x9iyYUw_LU91m0YPIOy1zG6-61_BGQKQ3EMyW4t7U1g/edit).  This should be sent by the TAM or the IE involved, and then forwarded to Finance as part of the [financial-wrapup workflow](/handbook/customer-success/implmentation-engineering/workflows/internal/financial-wrapup.html).