---
layout: markdown_page
title: "Backend Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Teams

There are a number of teams within the Backend group:

* [CI/CD](/handbook/backend#cicd)
* [Discussion](/handbook/backend#discussion)
* [Platform](/handbook/backend#platform)
* [Monitoring](/handbook/backend#monitoring)

Each team has a different focus on what issues to work on for each
release. The following information is not meant to be a set of hard-and-fast
rules, but as a guideline as to what team decides can best improve certain
areas of GitLab.

APIs should be shared responsibility between all teams within the
Backend group.

There is a backend group call every Tuesday, before the team call. You should
have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)

### CI/CD Team
{: #cicd}

The CI/CD Team is focused on all the functionality with respect to
Continuous Integration and Deployments.

This team maps to [Verify, Package, Release, and Configure](/handbook/product/categories/#ops).

### Discussion Team
{: #discussion}

The Discussion Team is focused on the collaboration functionality of GitLab.

This team maps to [Plan and Create](/handbook/product/categories/#dev).

### Platform Team
{: #platform}

The Platform Team is focused on all the other areas of GitLab that
the CI and Discussion Teams do not cover.

This team maps to [Create and Auth](/handbook/product/categories/#dev).

### Monitoring Team
{: #monitoring}

The monitoring team is responsible for:
* Provide the tools required to enable monitoring of GitLab.com
* Package these tools to enable all customers to manage their instances easily and completely
* Build integrated monitoring solutions for customers apps into GitLab, including: metrics, logging, and tracing

This team maps to [Monitor](/handbook/product/categories/#ops).

#### Process for adding new metrics to GitLab

The [Monitoring](/solutions/monitor/) team is responsible for providing the underlying libraries and tools to enable GitLab team members to instrument their code. When adding new metrics, we need to consider a few facets: the impact on GitLab.com, customer deployments, and whether any default alerting rules should be provided.

Recommended process for adding new metrics:

1. Open an issue in the desired project outlining the new metrics desired
1. Label with the ~Monitoring label, and ping @gl-monitoring for initial review
1. During implementation consider:
  1. The Prometheus [naming]((https://prometheus.io/docs/practices/naming/)) and [instrumentation](https://prometheus.io/docs/practices/instrumentation/) guidelines
  1. Impact on cardinality and performance of Prometheus
  1. Whether any alerts should be created
1. Assign to an available Monitoring team reviewer
